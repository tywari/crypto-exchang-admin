@extends('home.master')
@section('headerjs')
@endsection()

@section('content')
<!--===================== First Section ========================-->
<div class="first-section animatedParent" style="padding: 75px 0 50px 0; margin-bottom: 50px;" >
		<div class="container">
			
			<div class="row">
				<div class="col-xl-12">
					<div class="text text-center" style="max-width:100%" >
						<h1>Privacy Policy</h1>
					</div>
				</div>
			</div>
			
			<!--<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6 col-12 animated bounceInLeft">
					<div class="text">
						<h1>Privacy Policy</h1>
						<span class="d-flex align-items-center"><b class="line">&nbsp;</b>More like a family<b class="line">&nbsp;</b></span>
					</div>
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6 col-12 animated bounceInRight position-relative">
					<img src="wp-content/themes/cryptocurrency-html/assets/img/about-img.jpg" alt="about-img">					
				</div>
			</div>-->
			
		</div>
		<div class="cloud">&nbsp;</div>
		<div class="cloud-two">&nbsp;</div>
		<div class="mini-cloud"></div>
		<div class="mini-cloud two"></div>
		<div class="mini-cloud three"></div>
	</div>
	<!--===================== End of First Section ========================-->
	<div class="container">
		<div class="row">
			<div class="col-xl-12 text-about">
				<!--<h2 class="h2-main">Meet our company</h2>-->
				<p>
				We respects our users' privacy and guarantee that all personal information submitted to us (through the platform or otherwise) is secured in respect of confidentiality and no piece of such information is shared with any other party (unless explicitly solicited by us and permitted by users or required by law).
				</p>
				<p>&nbsp;</p>
				
				<p><strong>Legal Compliance</strong></p>
				<p>This website complies with all relevant laws for user privacy and this policy has been specifically instated pursuant to the provisions of the Information Technology (Reasonable security practices and procedures and sensitive personal data or information) Rules, 2011 issued by the Government of India in exercise of the powers conferred by clause (ob) of sub-section (2) of section 87 read with section 43A of the Information Technology Act, 2000 (21 of 2000).
				</p>
				<p>&nbsp;</p>
				
				<p><strong>Website Navigation</strong></p>
				<p>A user can view certain portions of the website without furnishing any personal information (open pages). The rest of the website has restricted access and details of those portions will only be visible to users after submitting personal information for identification/authentication purposes to grant access.
				</p>
				<p>&nbsp;</p>
				
				<p><strong>Signing Up</strong></p>
				<p>Users can register on Giltxchange by presenting the relevant information as required by the platform. The information collected by the users during signing up (Name, Email, Mobile Number, Identity Proofs etc.) is used to identify a user and this is needed to send requested information to users from time to time.
				</p>
				<p>&nbsp;</p>
				
				<p>We aim to offer the best services to our users and to analyze the performance of the platform some personal information of users could be used. This is strictly to enhance the quality of the services and improvement of the platform itself.
				</p>
				<p>&nbsp;</p>
				<p>Users can subscribe to the listed electronic communication services (newsletters, announcements etc.) which will result in such communications from Giltxchange. Users can choose to unsubscribe at any time and the option to do so will be available within all online communications.
				</p>
				<p>&nbsp;</p>
				<p> If users indicate that they do not want to furnish certain information, we do not solicit the same in any form. However, some services of Giltxchange platform may only be provided to the users who have submitted such personal information.
				</p>
				<p>&nbsp;</p>
				<p><strong>Information Security</strong></p>
				<p>We use strong security measures with the latest technology to safeguard the personal information collected and store it in a secure environment protected from unauthorized access, modification or disclosure. We ensure that the authorized use or disclosure from Giltxchange is accurate, complete and up-to-date. In any case of security or privacy concerns, users can contact us on support@giltxchange.com.
				</p>
				<p>&nbsp;</p>														
				<p>We add an additional layer of security on the collected information by encrypting it and storing it safely. Transport layer security mechanisms are employed to protect theft and leak of data in transit (none of it is tracked by Giltxchange). We also use two-factor authentication for all sensitive user actions on the platform. We add an additional layer of security to your data at rest in the cloud, using scalable and efficient encryption features.
				</p>
				<p>&nbsp;</p>														
				<p>We do not disclose customer content unless we’re required to do so to comply with the law or a valid and binding order of a governmental or regulatory body. Unless prohibited from doing so or there is clear indication of illegal conduct in connection with the use of Giltxchange products or services, Giltxchange notifies customers before disclosing customer content so they can seek protection from disclosure.
				</p>
				<p>&nbsp;</p>	
				<p>We cannot protect any information that users make available to general public (for example, on community chats). For reasons beyond our control, unforeseen security risks may arise and Giltxchange disclaims any responsibility for damages arising out of such a security breach or failure. Though we will put out best efforts to secure the information, in the event of a breach or failure, information transmitted to us or from our products and services will be at the users’ risk.
				</p>
				<p>&nbsp;</p>
				<p><strong>Information Sharing</strong></p>
				<p>All user information collected by us through the platform or the information generated by users’ activity on the platform is maintained confidentially and not consciously shared with any third parties other than in the following circumstances: (i) If required by governmental, regulatory or law enforcement agencies;
				</p>
				<p>&nbsp;</p>
				<p>If required by law to be supplied to any other entity;</p>
				<p>&nbsp;</p>
				<p>Shared with Giltxchange’s affiliate entities for marketing and advertising requirements in relation to our services;
				</p>
				<p>&nbsp;</p>
				<p>Anyone we transfer our business to in respect of which users become potential customers;
				</p>
				<p>&nbsp;</p>
				<p>Shared with affiliate entities which provide to us products and services which in turn support products and services that we provide.
				</p>
				<p>&nbsp;</p>
				<p><strong>Cookies</strong></p>
				<p>The Giltxchange website uses cookies to enhance the user experience while visiting the website. Cookies are small files saved on the users’ computer hard drives that track, save and store information about the users’ interactions and usage of the website. This allows the website, through its server to provide the users with a tailored experience within the website. Users are advised that if they wish to stop the use and saving of cookies from this website on to their computer hard drives, they should take the necessary steps within their own web browser’s security settings to block all cookies from this website and its external serving vendors.
				</p>
				<p>&nbsp;</p>
				<p>Other cookies may be stored to users’ computer hard drives by external vendors when their website uses referral program, sponsored links or adverts. Such cookies are used for conversion and referral tracking and typically expire after 30 days, though some may take longer. No personal information is stored, saved or collected other than with specific consent.
				</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				
				
			</div>
		</div>
		
		
		
		
		
	</div>
	<!--===================== About Working ========================-->
	<div class="about-working" style="display:none;" >
		<div class="container">
			<div class="row">
				<div class="col-xl-5">&nbsp;</div>
				<div class="col-xl-7 col-12">
					<div class="text-head">
						<h5>The Perks</h5>
						<h2 class="h2-main">About working here </h2>
					</div><!--text-head-->
					<ul class="list-unstyled animatedParent">
						<li class="d-flex align-items-center animated bounceInRight delay-250">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about.svg" alt="icon-about">
							<div class="content">
								<h5>Big office</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-500">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about2.svg" alt="icon-about">
							<div class="content">
								<h5>Greate co-workers</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-750">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about3.svg" alt="icon-about">
							<div class="content">
								<h5>Education opportunities</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-1000">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about4.svg" alt="icon-about">
							<div class="content">
								<h5>Soccialy responsible</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-1250">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about5.svg" alt="icon-about">
							<div class="content">
								<h5>Food</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
						<li class="d-flex align-items-center animated bounceInRight delay-1250">
							<img src="wp-content/themes/cryptocurrency-html/assets/img/icon-about6.svg" alt="icon-about">
							<div class="content">
								<h5>Team spirit</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--===================== End of About Working ========================-->
	<!--===================== Team Slider ========================-->
	<div class="container team-slider-text" style="display:none;" >
		<div class="row">
			<div class="col-xl-12">
				<div class="text-head text-center">
					<h5>Team</h5>
					<h2 class="h2-main">More like a family</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div><!--text-head-->
			</div>
		</div>
	</div>
	<div class="team-slider animatedParent" style="display:none;" >
		<div class="item animated bounceInRight delay-250">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Anna Gross</h5>
				<span>Project Manager</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
		<div class="item animated bounceInRight delay-500">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider2.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Mariya Norman</h5>
				<span>Sales Manager</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_15" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin1"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
		<div class="item animated bounceInRight delay-750">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider3.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Sem Smith</h5>
				<span>Director Business Development</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_14" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin5"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
		<div class="item animated bounceInRight delay-1000">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider4.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Rita Marvel</h5>
				<span>Graphic Designer</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_13" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin4"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
		<div class="item animated bounceInRight delay-1250">
			<img src="wp-content/themes/cryptocurrency-html/assets/img/team-slider3.jpg" alt="team-slider">
			<div class="info-team">
				<h5>Sem Smith</h5>
				<span>Director Business Development</span>
				<div class="line">&nbsp;</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
				<a href="index.html#"><svg version="1.1" id="Capa_12" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><g><g id="post-linkedin3"><path d="M510,0H0c0,0,0,23,0,51v408c0,28,0,51,0,51h510c0,0,0-23,0-51V51C510,23,510,0,510,0zM153,433.5H76.5V204H153V433.5zM114.8,160.6c-25.5,0-45.9-20.4-45.9-45.9s20.4-45.9,45.9-45.9s45.9,20.4,45.9,45.9S140.2,160.6,114.8,160.6z M433.5,433.5H357V298.4c0-20.4-17.9-38.2-38.2-38.2s-38.2,17.9-38.2,38.2v135.1H204V204h76.5v30.6c12.8-20.4,40.8-35.7,63.8-35.7c48.5,0,89.2,40.8,89.2,89.3V433.5z"/></g></g></svg></a>
			</div>
		</div>
	</div>
	<!--===================== End of Team Slider ========================-->
	<!--===================== Join Us ========================-->
	<div class="container join-us" style="display:none;" >
		<div class="row">
			<div class="col-xl-12">
				<div class="text-head text-center">
					<h5>Openings</h5>
					<h2 class="h2-main">Life is short. Join us</h2>
				</div><!--text-head-->
				<!--===================== Join Block ========================-->
				<ul class="animatedParent">
					<li class="d-flex justify-content-between align-items-center animated bounceInUp delay-250">
						<div class="inside">
							<h4>Data Scientist</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<a href="index.html#" class="see-brd-btn">Apply Now</a>
					</li>
					<li class="d-flex justify-content-between align-items-center animated bounceInUp delay-500">
						<div class="inside">
							<h4>Didgital Designer</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<a href="index.html#" class="see-brd-btn">Apply Now</a>
					</li>
					<li class="d-flex justify-content-between align-items-center animated bounceInUp delay-750">
						<div class="inside">
							<h4>Illustrator</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<a href="index.html#" class="see-brd-btn">Apply Now</a>
					</li>
					<li class="d-flex justify-content-between align-items-center animated bounceInUp delay-1000">
						<div class="inside">
							<h4>Data Scientist</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<a href="index.html#" class="see-brd-btn">Apply Now</a>
					</li>
				</ul>
				<!--===================== End of Join Block ========================-->
			</div>
		</div>
	</div>
	<!--===================== End of Join Us ========================-->
@endsection()
@section('footerjs')
    
    
@endsection()
