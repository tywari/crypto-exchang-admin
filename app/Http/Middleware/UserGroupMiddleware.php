<?php

namespace App\Http\Middleware;

use Closure;

class UserGroupMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('loggedin')) {
            return redirect('/secure/login')->send();  ;
        }
        return $next($request);
    }
}
