<?php

namespace App\Http\Controllers;

use App\Libraries\KrakenAPI;
use Illuminate\Http\Request;
use Validator;


class IndexController extends Controller {

    
    
    public function __construct(Request $request)
	{
        
    }
    
    public function index(){
        return view('home/index', []);
    }
    public function about(){
        return view('home/about', []);
    }
    public function faq(){
        return view('home/faq', []);
    }
    public function support(){
        return view('home/support', []);
    }
    public function contact(){
        return view('home/support', []);
    }

    public function aml(){
        return view('home/aml', []);
    }
    public function fee(){
        return view('home/fee', []);
    }
    public function terms(){
        return view('home/terms', []);
    }
    public function privacy(){
        return view('home/privacy', []);
    }
    public function disclaimer(){
        return view('home/disclaimer', []);
    }

    public function upload_file(){
        return view('home/file', []);
    }
    public function submit_kycTest(Request $request)
    {
        $this->s3_image_upload($request->file('aadhar_front'), 'uploads/kyc/',1);
        $this->s3_image_upload($request->file('aadhar_front1'), 'uploads/kyc/',2);
        $this->s3_image_upload($request->file('aadhar_front2'), 'uploads/kyc/',3);
        $this->s3_image_upload($request->file('aadhar_front3'), 'uploads/kyc/',4);
        //$this->s3_image_upload($request->file('aadhar_front4'), 'uploads/kyc/',5);
        echo json_encode(['adsf']);
    }
    private function s3_image_upload($image, $destinationPath, $c)
    {
        // $s3  = \Storage::disk('s3');

        // $ext = $file->getClientOriginalExtension();

        // $name= $user_id.time().'.'.$ext;

        // $path= '/careers/' . $name;

        // $s3->put($path, file_get_contents($file), 'public');

        // $url = \Storage::disk('s3')->url('careers/'.$name);

        // return $url;

        $custom_file_name = $c.'-'.time().'-'.$image->getClientOriginalName();
        //$path = $image->storeAs($destinationPath,$custom_file_name);
        
        
        $resp = $image->move($destinationPath, $custom_file_name);
        return $custom_file_name;
        

    }
    
    
    
    
}