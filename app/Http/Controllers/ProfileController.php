<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Libraries\User;
use App\Libraries\Trade;
use App\Traits\StatusResponse;
use Validator;


class ProfileController extends Controller {

    use StatusResponse;

    public function __construct(Request $request)
	{
        $this->middleware('group');
       
        $this->token = $request->session()->get('token');
        
        $this->user_id = User::get_user_id($this->token);
       
        $this->avalaible_coin = Trade::getAvailableCoins();
        $this->active_coin_pair = ['inr-xrp'=>0,'inr-btc'=>0,'inr-eth'=>0,'inr-bch'=>0, 'inr-ltc'=>0];

        $this->user = new User();
    }
    
    public function index(Request $request)
	{
        $d = [
            'id'=>$this->user_id,
            'is_kyc'=>true,
            'is_bank'=>true
        ];
        
        $user = User::userData($d);
        
        $data=[
            'active_coin_pair' =>$this->active_coin_pair,
            'user_data'=>$user
        ];
        // echo '<pre>';
        // print_r($data['user_data']);
        // die;
        return view('xchange/profile', $data);
    }

    public function submit_profile(Request $request)
    {
        $validator = Validator::make($request->all(), $this->profielRule(), $this->profielValidationMessages());
        if ($validator->fails()) {
            
            $data = $validator->getMessageBag()->toArray();
            $message = $validator->errors()->first();
            return $this->_status('RPI', NULL, $data);

        } else {
            
            $d=[
                'dob' => date('Y-m-d', strtotime($request->input('dob'))),
                'first_name'=>$request->input('first_name'),
                'last_name'=>$request->input('last_name'),
                'is_profile'=>1
            ];
            $data = [
                'table'=>'users',
                'data'=>$d,
                'where'=>['id'=>$this->user_id],
            ];
            $resp = User::save($data);
            return $this->_status('SUCC', NULL);
        }
    }
    private function s3_image_upload($image, $destinationPath)
    {
        // $s3  = \Storage::disk('s3');

        // $ext = $file->getClientOriginalExtension();

        // $name= $user_id.time().'.'.$ext;

        // $path= '/careers/' . $name;

        // $s3->put($path, file_get_contents($file), 'public');

        // $url = \Storage::disk('s3')->url('careers/'.$name);

        // return $url;

        $custom_file_name = time().'-'.$image->getClientOriginalName();
        //$path = $image->storeAs($destinationPath,$custom_file_name);
        
        
        $resp = $image->move($destinationPath, $custom_file_name);
        return $custom_file_name;
        

    }


    public function submit_kyc(Request $request)
    {
        $validator = Validator::make($request->all(), $this->kycRule(), $this->kycValidationMessages());
        if ($validator->fails()) {
            
            $data = $validator->getMessageBag()->toArray();
            $message = $validator->errors()->first();
            return $this->_status('RPI', NULL, $data);

        } else {
           
            $data = [
                'data'=>
                    ['user_id'=>$this->user_id,
                    'aadhar_no'=>$request->input('aadhar_no'),
                    'pan_no'=>$request->input('pan_no'),
                    'aadhar_front'=>$this->s3_image_upload($request->file('aadhar_front'), 'uploads/kyc/'),
                    'aadhar_back'=>$this->s3_image_upload($request->file('aadhar_back'), 'uploads/kyc/'),
                    'pan_img'=>$this->s3_image_upload($request->file('pan_img'), 'uploads/kyc/'),
                ],
                'where'=>['user_id'=>$this->user_id],
                'table'=>'user_kyc',
                
            ];
            
            $resp = User::saveUserData($data);

            $data = [
                'table'=>'users',
                'data'=>['is_kyc'=>1],
                'where'=>['id'=>$this->user_id],
            ];
            $resp = User::save($data);

            return $this->_status('SUCC', NULL);
        }
    }

    private function kycRule()
    {
        return [
            'aadhar_no' => 'required',
            'aadhar_front' => 'required||mimes:jpeg,png,jpg,pdf,html|max:2048',
            'aadhar_back' => 'required||mimes:jpeg,png,jpg,pdf,html|max:2048',
            'pan_no' => 'required',
            'pan_img' => 'required||mimes:jpeg,png,jpg,pdf,html|max:2048',
        ];
    }
    private function kycValidationMessages()
    {
        return [
            'aadhar_no.required' => 'Aadhar number required',
            'aadhar_front.required' => 'Kindly attach Aadhar front picture',
            'aadhar_back.required' => 'Kindly attach Aadhar back picture',
            'pan_no.required' => 'PAN no required',
            'pan_img.required' => 'Kindly attach PAN cart picture',
        ];
    }


    public function submit_bank(Request $request)
    {
        $validator = Validator::make($request->all(), $this->bankRule(), $this->bankValidationMessages());
        if ($validator->fails()) {
            
            $data = $validator->getMessageBag()->toArray();
            $message = $validator->errors()->first();
            return $this->_status('RPI', NULL, $data);

        } else {
            
            $b = $request->input();
            unset($b['account_no_confirmation']);
            $data = [
                
                    'table'=>'user_bank_accounts',
                    'data'=>$b,
                    'where'=>['user_id'=>$this->user_id],
                ];
            $resp = User::saveUserData($data);
            
            $data = [
                'table'=>'users',
                'data'=>['is_bank'=>1],
                'where'=>['id'=>$this->user_id],
            ];
            $resp = User::save($data);


            return $this->_status('SUCC', NULL);
        }
    }
    
    private function bankRule()
    {
        return [
            'ifsc' => 'required',
            'bank' => 'required',
            'account_no' => 'required|numeric|confirmed',
            'account_no_confirmation' => 'required|numeric',
            'linked_mobile' => 'required|numeric',
            'branch' => 'required',
            'account_type' => 'required|alpha',
            'holder_name' => 'required|string',
            
        ];
    }
    private function bankValidationMessages()
    {
        return [
            'ifsc.required' => 'IFSC could not be empty',
            'bank.required' => 'Bank name could not be empty',
            'account_no.required' => 'Enter your account no.',
            'confirm_acc.required' => 'Confirm account should be match with Account no',
            'linked_mobile.required' => 'Linked mobile no should be empty and valid',
            'branch.required' => 'Enter branch name',
            'account_type.required' => 'Enter account type',
            'holder_name.required' => 'Enter name of account holder',
        ];
    }
    private function profielRule()
    {
        return [
            'first_name' => 'required|alpha',
            'last_name' => 'required',
            'dob' => 'required',
        ];
    }
    private function profielValidationMessages()
    {
        return [
            'first_name.required' => 'First name could not be empty',
            'last_name.required' => 'Last name could not be empty',
            'dob.required' => 'Select your DOB',
        ];
    }

    public function set2FA(Request $request)
    {
        if (!$request->input('g2fa') || $request->input('g2fa')=='') {
            return $this->_status('CUST', NULL);
        } else{
            
            $d = [
                'auth_code'=>trim($request->input('g2fa')),
                'token'=>$this->token
            ];

            if($request->input('atcion')==1)
            {
                $url = '2fa/disable';
            }
            else
                $url = '2fa/set';
            

            echo $this->user->curlPostRequest($url, $d);
        }
    }

    public function reset_password(Request $request)
    {
        $validator = Validator::make($request->all(), $this->passwordRule(), $this->passwordValidationMessages());
        if ($validator->fails()) {
            
            $data = $validator->getMessageBag()->toArray();
            $message = $validator->errors()->first();
            return $this->_status('RPI', NULL, $data);

        } else {
            $d = [
                'new_password'=>trim($request->input('password')),
                'token'=>$this->token,
                'old_password'=>trim($request->input('old_password')),
            ];
            $url = 'password/reset';
            echo $this->user->curlPostRequest($url, $d);
        }
        

        
    }

    private function passwordRule()
    {
        return [
            'password' => 'required|confirmed',
            'old_password' => 'required',
            'password_confirmation' => 'required',
        ];
    }
    private function passwordValidationMessages()
    {
        return [
            'old_password.required' => 'Current Password should not be empty',
            'password.required' => 'Password should not be empty',
            'password_confirmation.required' => 'Confirm password should be of password',
            
        ];
    }

    

}